﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using UnityEngine;

public class ZipScript : MonoBehaviour
{
    private List<string> list;
    private string standalone_path = @"D:";
    private string android_path = Application.streamingAssetsPath;
    private string path;
    private void Start()
    {
        list = new List<string>();
        list.Add(Application.streamingAssetsPath + @"\data.json");
        list.Add(Application.streamingAssetsPath + @"\Cubemap_Black_White.jpg");
        path = CheckPlatform();
    }

    private string CheckPlatform()
    {
#if UNITY_ANDROID
            return android_path;
#elif UNITY_STANDALONE
        return standalone_path;
#endif
        return "";
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            CreateZipFile(path, list);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            UnpackZipFile(path, path);
        }
    }

    public void Zip()
    {
        CreateZipFile(path + "/ZipDemo.presenter", list);
    }

    public void Unzip()
    {
        UnpackZipFile(path + "/ZipDemo.presenter", path);
    }

    private void CreateZipFile(string filepath, IEnumerable<string> files)
    {
        var zip = ZipFile.Open(filepath, ZipArchiveMode.Create);
        foreach (var file in files)
        {
            zip.CreateEntryFromFile(file, Path.GetFileName(file), System.IO.Compression.CompressionLevel.Optimal);
        }
        zip.Dispose();
    }

    private void UnpackZipFile(string filepath, string outFolderName)
    {
        var zip = ZipFile.Open(filepath, ZipArchiveMode.Read);
        foreach (var file in zip.Entries)
        {
            file.ExtractToFile(outFolderName + "/" + file.Name);
        }
        zip.Dispose();
    }
}